import { createContext } from 'react';

const noop = () => {};

export const AuthContext = createContext({
	userId: null,
	messageText: 'auth context',
	setMessageText: noop,
	alert: false,
	setAlert: noop,
	severity: 'info',
	setSeverity: noop,
});
