import React, { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { AuthContext } from '../../context/AuthContext';
import Form from '../../common/Form/Form';
import { BUTTONS_TEXT } from '../../constants';
import { logIn } from '../../services';
import { userLogIn } from '../../store/user/actionCreators';

const Login = () => {
	const { setMessageText, setAlert, setSeverity } = useContext(AuthContext);
	const navigate = useNavigate();
	const [form, setForm] = useState({
		email: '',
		password: '',
	});
	const { LOGIN_LINK, LOGIN_BUTTON } = BUTTONS_TEXT;
	const dispatch = useDispatch();
	const storageName = 'userData';

	const loginHandler = async (event) => {
		event.preventDefault();
		if (!form.email && !form.password) {
			return;
		}

		logIn(form)
			.then((data) => {
				setMessageText('Success!');
				setSeverity('success');
				setAlert(false);
				localStorage.setItem(
					storageName,
					JSON.stringify({
						user: data.user,
						token: data.result,
					})
				);
				dispatch(userLogIn({ token: data.result, ...data.user }));
				navigate('/courses');
			})
			.catch((e) => {
				setMessageText(e.message);
				setSeverity('error');
				setAlert(false);
			});
	};

	const handleChange = (event) => {
		setForm({
			...form,
			[event.target.name]: event.target.value,
		});
	};

	return (
		<div className={'login'}>
			<Form
				linkText={LOGIN_LINK}
				onChange={handleChange}
				buttonText={LOGIN_BUTTON}
				onSubmit={loginHandler}
				linkPath={'/registration'}
				form={form}
			/>
		</div>
	);
};

export default Login;
