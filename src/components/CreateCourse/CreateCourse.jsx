import React, { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import UserItem from './components/UserItem/UserItem';
import { getDuration } from '../../helpers/pipeDuration';
import { BUTTONS_TEXT, LABEL_TEXT } from '../../constants';
import { AuthContext } from '../../context/AuthContext';
import { validateCourse } from '../../helpers/courseValidator';
import { getAuthors } from '../../store/authors/selectors';
import { addAuthorToCourse } from '../../store/authors/actionCreators';
import { addNewCourse } from '../../store/courses/actionCreators';
import { generateDate } from '../../helpers/dateGenerator';

import styles from './CreateCourse.module.css';

const CreateCourse = () => {
	const authorsList = useSelector(getAuthors);
	const [newAuthor, setNewAuthor] = useState({
		id: '',
		name: '',
	});
	const { CREATE_COURSE, CREATE_AUTHOR, ADD_AUTHOR, DELETE_AUTHOR } =
		BUTTONS_TEXT;
	const { TITLE, AUTHOR_NAME, DESCRIPTION, DURATION } = LABEL_TEXT;
	const [course, setCourse] = useState({
		id: '',
		title: 'title',
		description: 'description',
		creationDate: '',
		duration: 65,
		authors: [],
	});
	const { title, description, duration, authors } = course;
	const { setMessageText, setAlert, setSeverity } = useContext(AuthContext);
	const navigate = useNavigate();
	const dispatch = useDispatch();

	const addCourse = () => {
		const isValid = validateCourse(course);
		if (!isValid.success) {
			setMessageText(isValid.message);
			setSeverity('error');
			setAlert(false);
			return;
		}

		dispatch(
			addNewCourse({
				...course,
				duration: +course.duration,
				id: uuidv4(),
				creationDate: generateDate(),
			})
		);

		setMessageText('New course has been added successfully!');
		setSeverity('success');
		setAlert(false);
		navigate('/courses');
		setCourse({
			id: '',
			title: '',
			description: '',
			creationDate: '',
			duration: 0,
			authors: [],
		});
	};

	const addAuthor = (author) => {
		if (course.authors.includes(author.id)) {
			setMessageText('This author has been already added!');
			setSeverity('warning');
			setAlert(false);
			return;
		}
		setCourse({
			...course,
			authors: [...course.authors, author.id],
		});

		setMessageText(`${author.name} has been added successfully!`);
		setSeverity('success');
		setAlert(false);
	};

	const deleteAuthor = (author) => {
		const result = course.authors.filter((authorId) => {
			return authorId !== author.id;
		});

		setCourse({
			...course,
			authors: result,
		});

		setMessageText(`${author.name} has been removed successfully!`);
		setSeverity('success');
		setAlert(false);
	};

	const handleChangeCourse = (event) => {
		setCourse({
			...course,
			[event.target.name]: event.target.value,
		});
	};

	const createAuthor = async () => {
		if (!newAuthor.name.trim() || newAuthor.name.trim().length <= 2) {
			setMessageText('Invalid Data!');
			setSeverity('warning');
			setAlert(false);
			return;
		}
		dispatch(addAuthorToCourse({ ...newAuthor, id: uuidv4() }));
		setNewAuthor({
			id: '',
			name: '',
		});
		setMessageText(`You have added ${newAuthor.name} successfully!`);
		setSeverity('success');
		setAlert(false);
	};

	return (
		<div className={styles.container}>
			<div className={styles.description__content}>
				<Input
					name='title'
					value={title}
					onChange={handleChangeCourse}
					labelText={TITLE}
					placeholderText={TITLE}
				/>
				<Button buttonText={CREATE_COURSE} callback={addCourse} />
			</div>
			<div className={styles.description__content}>
				<label className={styles.label} htmlFor='textarea'>
					{DESCRIPTION}
				</label>
				<textarea
					className={styles.textarea}
					name='description'
					id='textarea'
					rows='5'
					value={description}
					placeholder={DESCRIPTION}
					onChange={handleChangeCourse}
				/>
			</div>
			<div className={styles.authors__content}>
				<div className={styles.left__box}>
					<div className={styles.add__author}>
						<h2>Add Author</h2>
						<Input
							value={newAuthor.name}
							placeholderText={AUTHOR_NAME}
							labelText={AUTHOR_NAME}
							name={'newAuthor'}
							onChange={(event) =>
								setNewAuthor({ ...newAuthor, name: event.target.value })
							}
						/>
						<Button
							callback={createAuthor}
							buttonText={CREATE_AUTHOR}
							className={'blue'}
						/>
					</div>
					<div className={styles.duration}>
						<h2>{DURATION}</h2>
						<Input
							name='duration'
							value={duration}
							placeholderText={DURATION}
							labelText={DURATION}
							onChange={handleChangeCourse}
							type='number'
						/>
						<p className={styles.duration__value}>
							{DURATION}: <strong>{getDuration(duration)}</strong> hours
						</p>
					</div>
				</div>
				<div className={styles.right__box}>
					<div className={styles.authors}>
						<h2>Authors</h2>
						{authorsList.map((author) => {
							return (
								<UserItem
									key={author.id}
									buttonText={ADD_AUTHOR}
									cb={addAuthor}
									author={author}
									className={'green'}
								/>
							);
						})}
					</div>
					<div className={styles.authors__list}>
						<h2>Course authors</h2>
						{!authors.length && <p>Authors list is empty</p>}
						{!!authors.length &&
							authors.map((authorId) => {
								const author = authorsList.find((element) => {
									return element.id === authorId;
								});
								return (
									<UserItem
										key={author.id}
										buttonText={DELETE_AUTHOR}
										cb={deleteAuthor}
										author={author}
										className={'red'}
									/>
								);
							})}
					</div>
				</div>
			</div>
		</div>
	);
};

export default CreateCourse;
