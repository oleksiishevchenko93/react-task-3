import React from 'react';
import PropTypes from 'prop-types';

import Button from '../../../../common/Button/Button';

import styles from './UserItem.module.css';

const UserItem = ({ author, cb, buttonText, className }) => {
	return (
		<div className={styles.container}>
			<p className={styles.user}> {author.name}</p>
			<Button
				buttonText={buttonText}
				callback={() => cb(author)}
				className={className}
			/>
		</div>
	);
};

UserItem.propTypes = {
	author: PropTypes.object.isRequired,
	cb: PropTypes.func.isRequired,
	buttonText: PropTypes.string.isRequired,
	className: PropTypes.string,
};

export default UserItem;
