import React, { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { getAuthorsData } from '../../helpers/authorsGetter';
import { changeDate } from '../../helpers/dateGenerator';
import Button from '../../common/Button/Button';
import { getDuration } from '../../helpers/pipeDuration';
import { getAuthors } from '../../store/authors/selectors';
import { getCourses } from '../../store/courses/selectors';

import styles from './CourseInfo.module.css';

const CourseInfo = () => {
	const { courseId } = useParams();
	const authorsList = useSelector(getAuthors);
	const courses = useSelector(getCourses);
	const navigate = useNavigate();
	const [course, setCourse] = useState({});
	const [ready, setReady] = useState(false);

	useEffect(() => {
		const courseById = courses.find((course) => course.id === courseId);
		setCourse(courseById);
		setReady(true);
	}, [courseId, courses]);

	if (!ready) {
		return (
			<div>
				<h2>Loading...</h2>
			</div>
		);
	}

	return (
		<div className={styles.courseInfo}>
			<Button
				buttonText={'Back to courses'}
				callback={() => navigate(`/courses`)}
				className={'blue'}
			/>
			<h1 className={styles.title}>{course.title}</h1>
			<div className={styles.content}>
				<div className={styles.description}>
					<p>{course.description}</p>
				</div>
				<div className={styles.detail}>
					<p>
						<strong>ID: </strong>
						{course.id}
					</p>
					<p>
						<strong>Duration: </strong>
						{getDuration(course.duration)} hours
					</p>
					<p>
						<strong>Created: </strong>
						{changeDate(course.creationDate)}
					</p>
					<p>
						<strong>Authors: </strong>
					</p>
					{getAuthorsData(course.authors, authorsList).map((author, index) => {
						return (
							<p key={index} className={styles.author}>
								{author}
							</p>
						);
					})}
				</div>
			</div>
		</div>
	);
};

export default CourseInfo;
