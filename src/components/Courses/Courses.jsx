import React, { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';
import { BUTTONS_TEXT } from '../../constants';
import { AuthContext } from '../../context/AuthContext';
import { getCourses } from '../../store/courses/selectors';
import { getAuthors } from '../../store/authors/selectors';

import styles from './Courses.module.css';

const Courses = () => {
	const authorsList = useSelector(getAuthors);
	const coursesList = useSelector(getCourses);
	const [searchValue, setSearchValue] = useState('');
	const [searchCoursesList, setSearchCoursesList] = useState([]);
	const [search, setSearch] = useState(false);
	const { setMessageText, setSeverity, setAlert } = useContext(AuthContext);
	const { ADD_COURSE } = BUTTONS_TEXT;
	const navigate = useNavigate();

	const searchCourses = () => {
		if (!searchValue.trim()) {
			return setSearch(false);
		}
		const result = coursesList.filter((course) => {
			return (
				course.title.toLowerCase().includes(searchValue.toLowerCase()) ||
				course.id.toLowerCase().includes(searchValue.toLowerCase())
			);
		});
		setSearchCoursesList(result);
		setSearch(true);
	};

	const handleChange = (event) => {
		setSearchValue(event.target.value);
		if (!event.target.value.trim()) {
			setMessageText('Empty Value!');
			setSeverity('warning');
			setAlert(false);
			setSearch(false);
		}
	};

	const showInfoCourse = (course) => {
		navigate(`/courses/${course.id}`);
	};

	const toggleCreatePage = () => {
		navigate('/courses/add');
	};

	return (
		<div className={styles.courses}>
			<div className={styles.topBar}>
				<SearchBar
					onClick={searchCourses}
					onChange={handleChange}
					value={searchValue}
				/>
				<Button
					callback={toggleCreatePage}
					buttonText={ADD_COURSE}
					className={'green'}
				/>
			</div>
			{!search &&
				coursesList.map((course) => {
					return (
						<CourseCard
							key={course.id}
							course={course}
							authorsList={authorsList}
							showInfoCourse={showInfoCourse}
						/>
					);
				})}
			{search &&
				searchCoursesList.map((course) => {
					return (
						<CourseCard
							key={course.id}
							course={course}
							authorsList={authorsList}
							showInfoCourse={showInfoCourse}
						/>
					);
				})}
		</div>
	);
};

export default Courses;
