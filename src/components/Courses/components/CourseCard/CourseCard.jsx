import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { getDuration } from '../../../../helpers/pipeDuration';
import { changeDate } from '../../../../helpers/dateGenerator';
import { getLimitAuthors } from '../../../../helpers/authorsGetter';
import { deleteCourse } from '../../../../store/courses/actionCreators';
import { BUTTONS_TEXT } from '../../../../constants';
import { AuthContext } from '../../../../context/AuthContext';
import Button from '../../../../common/Button/Button';

import styles from './CourseCard.module.css';

const CourseCard = ({ course, authorsList }) => {
	const { title, description, creationDate, duration, authors } = course;
	const { SHOW_COURSE } = BUTTONS_TEXT;
	const { setMessageText, setAlert, setSeverity } = useContext(AuthContext);
	const dispatch = useDispatch();

	const deleteCourseById = (courseId) => {
		dispatch(deleteCourse(courseId));

		setMessageText('This course has been removed successfully!');
		setSeverity('success');
		setAlert(false);
	};

	return (
		<div className={styles.courseCard}>
			<div className={styles.courseCard__content}>
				<h2>{title}</h2>
				<p>{description}</p>
			</div>
			<div className={styles.courseCard__info}>
				<p>
					<strong>Authors: </strong>
					{getLimitAuthors(authors, authorsList)}
				</p>
				<p>
					<strong>Duration: </strong>
					{getDuration(duration)} hours
				</p>
				<p>
					<strong>Created: </strong>
					{changeDate(creationDate)}
				</p>
				<Link to={`/courses/${course.id}`} className={'link blue'}>
					{SHOW_COURSE}
				</Link>
				<Button
					buttonText={'update'}
					className={'green'}
					callback={() => {
						setMessageText('This course has been updated successfully!');
						setSeverity('success');
						setAlert(false);
					}}
				/>
				<Button
					buttonText={'Delete'}
					className={'red'}
					callback={() => deleteCourseById(course.id)}
				/>
			</div>
		</div>
	);
};

CourseCard.propTypes = {
	course: PropTypes.object.isRequired,
	authorsList: PropTypes.array.isRequired,
};

export default CourseCard;
