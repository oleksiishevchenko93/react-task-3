import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import { AuthContext } from '../../context/AuthContext';
import { useHttp } from '../../hooks/HttpHook';
import { userLogOut } from '../../store/user/actionCreators';
import { getUser as userSelector } from '../../store/user/selectors';
import { storageName, url } from '../../constants';

import styles from './Header.module.css';

const Header = () => {
	const { setMessageText, setAlert, setSeverity } = useContext(AuthContext);
	const { request, loading } = useHttp();
	const [user, setUser] = useState({});
	const dispatch = useDispatch();
	const userData = useSelector(userSelector);
	const [ready, setReady] = useState(false);

	const getUser = useCallback(async () => {
		try {
			const data = await request(`${url}/users/me`, 'GET', null, {
				Authorization: userData.token,
			});
			setUser(data.result);
			setReady(true);
		} catch (e) {
			setMessageText(e.message);
			setSeverity('error');
			setAlert(false);
		}
	}, [userData.token, request]);

	const logout = () => {
		dispatch(userLogOut());
		localStorage.removeItem(storageName);
	};

	useEffect(() => {
		if (userData.token) {
			getUser();
		}
	}, [getUser, userData.token]);

	if (loading || !ready) {
		return <h2>Loading...</h2>;
	}

	return (
		<div className={styles.header}>
			<Logo />
			<div className={styles.userName}>
				<h2>{user.name || 'Admin'}</h2>
				<Button buttonText='log out' className={'green'} callback={logout} />
			</div>
		</div>
	);
};

export default Header;
