import { ADD_COURSE, DELETE_COURSE, GET_COURSES } from './actionTypes';

const coursesInitialState = [];

export const coursesReducer = (state = coursesInitialState, action) => {
	switch (action.type) {
		case ADD_COURSE:
			return [...state, action.payload];
		case GET_COURSES:
			return action.payload;
		case DELETE_COURSE:
			return state.filter((course) => course.id !== action.payload);
		default:
			return state;
	}
};
