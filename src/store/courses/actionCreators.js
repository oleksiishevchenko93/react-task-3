import { ADD_COURSE, DELETE_COURSE, GET_COURSES } from './actionTypes';

export const setCourses = (courses) => {
	return {
		type: GET_COURSES,
		payload: courses,
	};
};

export const addNewCourse = (course) => {
	return {
		type: ADD_COURSE,
		payload: course,
	};
};

export const deleteCourse = (courseId) => {
	return {
		type: DELETE_COURSE,
		payload: courseId,
	};
};
