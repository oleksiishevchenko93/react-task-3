import { LOGIN, LOGOUT } from './actionTypes';

export const userLogIn = (form) => {
	return {
		type: LOGIN,
		payload: form,
	};
};

export const userLogOut = () => {
	return {
		type: LOGOUT,
	};
};
