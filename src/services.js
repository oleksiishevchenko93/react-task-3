import { url } from './constants';

const request = async (url, method = 'GET', body = null, headers = {}) => {
	try {
		if (body) {
			body = JSON.stringify(body);
			headers['Content-Type'] = 'application/json';
		}

		const response = await fetch(url, {
			method,
			body,
			headers,
		});

		const data = await response.json();

		if (!response.ok) {
			throw new Error(
				data.errors || data.result || data.message || 'Something went wrong...'
			);
		}

		return data;
	} catch (e) {
		throw e;
	}
};

export const fetchedCourses = async (token) => {
	try {
		const data = await request(`${url}/courses/all`, 'GET', null, {
			Authorization: token,
		});
		return data.result;
	} catch (e) {
		return e.message;
	}
};

export const fetchedAuthors = async (token) => {
	try {
		const data = await request(`${url}/authors/all`, 'GET', null, {
			Authorization: token,
		});
		return data.result;
	} catch (e) {
		return e.message;
	}
};

export const logIn = async (form) => {
	try {
		return await request(`${url}/login`, 'POST', { ...form });
	} catch (e) {
		return e.message;
	}
};
