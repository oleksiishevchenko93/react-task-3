import React, {useEffect, useState} from 'react';
import { BrowserRouter } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { AuthContext } from './context/AuthContext';
import { useMessage } from './hooks/MessageHook';
import { Snackbars } from './common/Alert/Alert';
import { useRoutes } from './routes';
import Header from './components/Header/Header';
import { fetchedAuthors, fetchedCourses } from './services';
import { getUser } from './store/user/selectors';
import { setAuthors } from './store/authors/actionCreators';
import { setCourses } from './store/courses/actionCreators';
import { userLogIn } from './store/user/actionCreators';
import { storageName } from './constants';

import './App.css';

function App() {
	const {
		messageText,
		setMessageText,
		alert,
		setAlert,
		severity,
		setSeverity,
	} = useMessage();
	const dispatch = useDispatch();
	const userData = useSelector(getUser);
	const isAuthenticated = userData.isAuth;
	const routes = useRoutes(isAuthenticated);
	const [ready, setReady] = useState(false);

	useEffect(() => {
		fetchedAuthors(userData.token)
			.then((authors) => {
				return dispatch(setAuthors(authors));
			})
			.catch((e) => {
				setSeverity('error');
				setMessageText(e.message);
				setAlert(false);
			});
		fetchedCourses(userData.token)
			.then((courses) => {
				return dispatch(setCourses(courses));
			})
			.catch((e) => {
				setSeverity('error');
				setMessageText(e.message);
				setAlert(false);
			});
	}, []);

	useEffect(() => {
		const data = JSON.parse(localStorage.getItem(storageName));

		if (data && data.token) {
			dispatch(userLogIn({ token: data.token, ...data.user }));
		}
		setReady(true);
	}, []);

	useEffect(() => {
		setAlert(true);
	}, [alert]);

	return (
		<AuthContext.Provider
			value={{
				messageText,
				setMessageText,
				alert,
				setAlert,
				severity,
				setSeverity,
			}}
		>
			<div className='container'>
				{isAuthenticated && <Header />}
				<BrowserRouter>{ready && routes}</BrowserRouter>
				{alert && (
					<Snackbars severity={severity} message={messageText} show={alert} />
				)}
			</div>
		</AuthContext.Provider>
	);
}

export default App;
